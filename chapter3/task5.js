String.prototype.reverse = function (){
    let string = "";
    let long = this.length;
    while(long){
        string += this[--long];
    }
    return string;
}
console.log("abcdef".reverse());