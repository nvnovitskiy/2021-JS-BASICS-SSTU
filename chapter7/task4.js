const list = document.getElementById("Список");
const input = document.getElementById("input");
document.getElementById("addButton").addEventListener("click", () => {
    if (!input.value) {
        return;
    }

    const item = document.createElement("ul");

    item.innerText = input.value;
    list.appendChild(item);
    input.value = "";
});
