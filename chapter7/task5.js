function createTable(operator) {
  let highlightedElement = null;
  const table = document.createElement("table");
  const thead = document.createElement("thead");
  const tbody = document.createElement("tbody");
  table.appendChild(thead);
  table.appendChild(tbody);
  const headerRow = document.createElement("tr");
  thead.appendChild(headerRow);
  const operatorEl = document.createElement("th");
  operatorEl.innerText = operator;
  headerRow.appendChild(operatorEl);
  for (let i = 1; i < 11; i++) {
    const headerEl = document.createElement("th");
    headerEl.innerHTML = i;
    headerRow.appendChild(headerEl);
    const trow = document.createElement("tr");
    const numEl = document.createElement("td");
    numEl.innerHTML = i;
    trow.appendChild(numEl);
    for (let j = 1; j < 11; j++) {
      const resEl = document.createElement("td");
      if ("+" === operator) {
        resEl.innerHTML = (i + j);
      } else {
        resEl.innerHTML = (i * j);
      }
      trow.appendChild(resEl);
    }
    tbody.appendChild(trow);
  }
  table.addEventListener("mousemove", (e) => {
    const target = e.target;
    if (!["TD", "TH"].includes(e.target.tagName)) {
      return;
    }
    if (highlightedElement) {
      highlightedElement.classList.remove("highlight");
    }
    highlightedElement = target;
    highlightedElement.classList.add("highlight");
  });
  table.addEventListener("mouseleave", () => {
    if (!highlightedElement) {
      return;
    }
    highlightedElement.classList.remove("highlight");
    highlightedElement = null;
  });
  return table;
}
document.body.appendChild(createTable("+"));
document.body.appendChild(createTable("*"));
