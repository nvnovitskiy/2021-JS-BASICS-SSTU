let one = '' + 1 + 0;
let two = '' - 1;
let three = true + false;
let four = 6 / '3';
let five = '2' +'3';
let six = 4 + 5 + 'px';
let seven = '$' + 4 + 5;
let eight = '4' - 2;
let nine = '4px' - 2;
let ten = 7 / 0;
let eleven = '  - 9  ' + 5;
let twelve = '  - 9  ' - 5;
let thirteen = null + 1;
let fourteen = undefined + 1;
let fifteen = (12 < 5) || !('one' === 'one') && (24 > 0);
let sixteen = ('text'.length === 4);

console.log(typeof(one), one);
console.log(typeof(two), two);
console.log(typeof(three), three);
console.log(typeof(four), four);
console.log(typeof(five), five);
console.log(typeof(six), six);
console.log(typeof(seven), seven);
console.log(typeof(eight), eight);
console.log(typeof(nine), nine);
console.log(typeof(ten), ten);
console.log(typeof(eleven), eleven);
console.log(typeof(twelve), twelve);
console.log(typeof(thirteen), thirteen);
console.log(typeof(fourteen), fourteen);
console.log(typeof(fifteen), fifteen);
console.log(typeof(sixteen), sixteen);



