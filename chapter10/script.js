const fields = [
    "no",
    "name",
    "position",
    "subjects",
    "education",
    "qualification",
    "degree",
    "academicRank",
    "profession",
    "trainings",
    "totalExperience",
    "professionExperience",
];

const FILTERS_KEY = "filters";
const PAGE_SIZE_KEY = "pageSize";

function loadUsers() {
    return new Promise((resolve) => {
        setTimeout(() => resolve(employees), 3000);
    });
}

function addHeader() {
    const headersTxtMap = {
        no: "#",
        name: "ФИО",
        position: "Должность",
        subjects: "Преподаваемые дисциплины",
        education: "Уровень образования",
        qualification: "Квалификация",
        degree: "Учёная степень",
        academicRank: "Учёное звание",
        profession: "Направление подготовки и (или) специальности",
        trainings: "Повышение квалификации",
        totalExperience: "Общий стаж",
        professionExperience: "Стаж работы по специальности",
    };

    const thead = employeesTable.createTHead();

    function addHeadCell(key) {
        const th = document.createElement("th");
        th.innerHTML = headersTxtMap[key];
        th.setAttribute("itemprop", key);

        thead.appendChild(th);
        th.onclick = () => {
            if (sort && sort.key === key) {
                sort.ord = sort.ord === "ASC" ? "DESC" : "ASC";
            } else {
                sort = {};
                sort.key = key;
                sort.ord = "ASC";
            }

            fillTable(findEmployees());
        };
    }

    for (const key of fields) {
        addHeadCell(key);
    }
}

function addFilters(filters) {
    const thead = employeesTable.tHead;
    const tr = document.createElement("tr");

    for (const key of fields) {
        const input = document.createElement("input");
        input.type = "input";
        input.name = key;
        input.form = "form";
        input.value = filters[key] || "";

        const th = document.createElement("th");
        th.setAttribute("itemprop", key);
        th.appendChild(input);
        tr.appendChild(th);

        input.addEventListener("input", () => {
            filters[key] = input.value;
            paging.page = 0;
            fillTable(findEmployees(employees, filters));
        });
    }

    thead.appendChild(tr);
}

function findEmployees() {
    console.time("фильтрация");
    saveFilters(filters);

    let filteredEmployees = employees.filter((employee) =>
        Object.keys(filters).every(
            (key) =>
                !filters[key] || new RegExp(filters[key], "i").test(employee[key])
        )
    );

    if (sort) {
        filteredEmployees = filteredEmployees.sort((a, b) => {
            const fieldA = a[sort.key];
            const fieldB = b[sort.key];
            const k = sort.ord === "ASC" ? 1 : -1;

            if (typeof fieldA === "string") {
                return fieldA.localeCompare(fieldB) * k;
            } else {
                return (fieldA - fieldB) * k;
            }
        });
    }
    paging.count = filteredEmployees.length;

    console.timeEnd("фильтрация");

    return filteredEmployees.slice(
        paging.page * paging.pageSize,
        (paging.page + 1) * paging.pageSize
    );
}

function fillTable(employees) {
    console.time("отрисовка");

    let tbody = employeesTable.querySelector("tbody");

    if (tbody) {
        tbody.remove();
    }
    tbody = employeesTable.createTBody();

    for (let i = 0; i < employees.length; i++) {
        const user = employees[i];
        const trow = document.createElement("tr");
        for (let key of fields) {
            const td = document.createElement("td");
            td.setAttribute("itemprop", key);
            td.textContent = user[key];
            trow.appendChild(td);
        }

        tbody.appendChild(trow);
    }

    addPagerToTable(employees);

    console.timeEnd("отрисовка");
}

function highlightRow() {
    let highlightedElement = null;

    employeesTable.addEventListener("mousemove", (e) => {
        const target = e.target;
        if (!["TD"].includes(e.target.tagName)) {
            return;
        }

        if (highlightedElement) {
            highlightedElement.classList.remove("highlight");
        }

        highlightedElement = target.parentElement;
        highlightedElement.classList.add("highlight");
    });

    employeesTable.addEventListener("mouseleave", () => {
        if (!highlightedElement) {
            return;
        }

        highlightedElement.classList.remove("highlight");
        highlightedElement = null;
    });
}

function addPagerToTable() {
    const numPages = Math.ceil(paging.count / paging.pageSize);

    const tfoot = employeesTable.createTFoot();
    tfoot.innerHTML = "";
    tfoot.insertRow().innerHTML = `<th class="footer" colspan="12"><div class="nav"></div></th>`;
    const nav = document.querySelector(".nav");
    nav.innerHTML = `
  <div>
    <select class="select">
      ${[20, 50, 100, 200, 1000].map(
        (value) =>
            `<option value="${value}" ${
                value === paging.pageSize ? " selected" : ""
            }>${value}</option>`
    )}
    </select>
  </div>
  ${Array.from({length: numPages}).map((_, i) => {
        return i === paging.page
            ? `<b>${i + 1}</b>`
            : `<a href="#" rel="${i}">${i + 1}</a>`;
    })}
  `;

    document.querySelector(".select").addEventListener("change", (e) => {
        paging.pageSize = e.target.value;
        localStorage.setItem(PAGE_SIZE_KEY, paging.pageSize);
        paging.page = 0;

        fillTable(findEmployees());
    });

    for (let navA of employeesTable.querySelectorAll(".nav a"))
        navA.addEventListener("click", (e) => {
            paging.page = parseInt(e.target.innerHTML) - 1;

            fillTable(findEmployees());
        });
}

function saveFilters(filters) {
    localStorage.setItem(FILTERS_KEY, JSON.stringify(filters));
}

function restoreFilters() {
    const filters = localStorage.getItem(FILTERS_KEY);
    return filters ? JSON.parse(filters) : {};
}

const employeesTable = document.getElementById("employees");
const filters = restoreFilters();
let sort = {
    key: "no",
    ord: "ASC",
};

let paging = {
    page: 0,
    pageSize: localStorage.getItem(PAGE_SIZE_KEY) || 20,
    count: 0,
};

addHeader();
addFilters(filters);
highlightRow();
loadUsers().then((users) => {
    fillTable(findEmployees(users));
});
