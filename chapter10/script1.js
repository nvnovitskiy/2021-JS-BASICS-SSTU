const TABLE_SELECTOR = "#w0 > table";
const getSelector = (name) => `[itemprop="${name}"]`;

const props = {
  name: "fio",
  position: "post",
  subjects: "teachingDiscipline",
  education: "teachingLevel",
  qualification: "teachingQual",
  degree: "degree",
  academicRank: "academStat",
  profession: "employeeQualification",
  trainings: "profDevelopment",
};

const [header, ...employeesElements] = document.querySelector(
  TABLE_SELECTOR
).rows;

const employees = employeesElements.map((employeeElement, index) => ({
  ...Object.keys(props).reduce((acc, key) => {
    acc[key] = employeeElement.querySelector(
      getSelector(props[key])
    ).textContent;
    return acc;
  }, {}),
  no: parseInt(employeeElement.firstChild.textContent),
  totalExperience: parseFloat(
    employeeElement.querySelector(getSelector("genExperience")).textContent
  ),
  professionExperience: parseFloat(
    employeeElement.querySelector(getSelector("specExperience")).textContent
  ),
}));

copy(employees);
