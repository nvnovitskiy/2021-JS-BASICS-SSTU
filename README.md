# Основы программирования на языке JavaScript (Самарский Государственный Технический Университет)
____

# Лабораторные работы:

 № Л/р | Название | Статус| Ссылка
 ----- |----------|-------|------
 1 | Основы языка JavaScript | ✅| [Ссылка](https://github.com/nvnovitskiy/2021-JS-BASICS-SSTU/tree/main/chapter1)
 2 | Условные операторы и циклы | ✅ | [Ссылка](https://github.com/nvnovitskiy/2021-JS-BASICS-SSTU/tree/main/chapter2)
 3 | Объекты | ✅ | [Ссылка](https://github.com/nvnovitskiy/2021-JS-BASICS-SSTU/tree/main/chapter3)
 4 | Массивы | ✅ | [Ссылка](https://github.com/nvnovitskiy/2021-JS-BASICS-SSTU/tree/main/chapter4)
 5 | Функции | ✅ | [Ссылка](https://github.com/nvnovitskiy/2021-JS-BASICS-SSTU/tree/main/chapter5)
 6 | Document Object Model | ✅ | [Ссылка](https://github.com/nvnovitskiy/2021-JS-BASICS-SSTU/tree/main/chapter6) 
 7 | События | ✅ | [Ссылка](https://github.com/nvnovitskiy/2021-JS-BASICS-SSTU/tree/main/chapter7)
 8 | Встроенные объекты Math и Date | ✅ | [Ссылка](https://github.com/nvnovitskiy/2021-JS-BASICS-SSTU/tree/main/chapter8) 
 9 | Инструменты JavaScript | ✅ | [Ссылка](https://github.com/nvnovitskiy/2021-JS-BASICS-SSTU/tree/main/chapter9)  
10 | Справочник сотрудников (проект) | ✅ | [Ссылка](https://github.com/nvnovitskiy/2021-JS-BASICS-SSTU/tree/main/chapter10)  
