let date = new Date('February 7, 1999 5:45:30');

function func(item){
    item = item.toString();
    if (item.length < 2){
        return "0" + item;
    }
    else{
        return item;
    }
}

function formatDate(date){
    let dd = func(date.getDate());
    let mm = func(date.getMonth() + 1);
    let yy = date.getFullYear();
    let hh = func(date.getHours());
    let min = func(date.getMinutes());
    return `${dd}.${mm}.${yy} ${hh}:${min}`;
}

console.log(formatDate(date));