let cars = [
    {
        model: "B34",
        year: 2002,
    },
    {
        model: "D68",
        year: 1555,
    },
    {
        model: "D67",
        year: 1332,
    },
    {
        model: "A98",
        year: 2011,
    },
    {
        model: "A35",
        year: 1923,
    }
];

function compareByModelAsc(car1, car2) {
    return car1.model.localeCompare(car2.model);
}

function compareByModelDesc(car1, car2) {
    return -car1.model.localeCompare(car2.model);
}

console.log([...cars].sort(compareByModelAsc));
console.log([...cars].sort(compareByModelDesc));
console.log([...cars].sort((car1, car2) => car1.year - car2.year));
console.log([...cars].sort((car1, car2) => car2.year - car1.year));