let area = (a, b) => {
    return a < 0 || b < 0 ? null : a * b;
};

console.log(area(5, 4));