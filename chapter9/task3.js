if(localStorage.getItem("userName") === null) {
    let name = prompt("Your name");
    localStorage.setItem('userName', name);
} else {
    alert(`Hello, ${localStorage.getItem('userName')}!`);
}