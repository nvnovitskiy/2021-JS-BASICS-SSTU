function printLessons(startDate, endDate) {
    fetch(`https://lk.samgtu.ru/api/common/distancelearning?${new URLSearchParams({
        start: new Date(startDate).toISOString(),
        end: new Date(endDate).toISOString()
    })}`)
        .then(response => response.json())
        .then(lessons => {
            console.info(`Количество пар: ${lessons.length}`);
            console.table(lessons.map(l => ({title: l.title, start: new Date(l.start).toLocaleString(),
                end: new Date(l.end).toLocaleString()})))
        })
}

printLessons("2021-03-29T00:00:00+03:00", "2021-04-27T00:00:00+03:00");