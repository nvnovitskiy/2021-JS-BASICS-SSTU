function printCookies (cookie) {
    let arr = cookie.split(';');
    arr.forEach(function(item, i, arr) {
        console.log(i + ' ' + item);
    });
}

document.cookie = "token=test; max-age=10";
printCookies(document.cookie);