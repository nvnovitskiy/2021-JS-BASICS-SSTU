let regExp = new RegExp(/^[А-ЯA-Z][а-яa-zА-ЯA-Z\-]*\s[А-ЯA-Z][а-яa-zА-ЯA-Z\-]+/);
let username = prompt("Enter name and surname: ");
try {
    if (username.match(regExp)) {
        alert("Valid Input!");
    } else {
        throw new Error("Invalid Input!");
    }
} catch (e) {
    alert(e.message);
}