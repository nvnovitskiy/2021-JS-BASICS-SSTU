let table, row, cell, i, j;
table = document.createElement('table');
table.style.textAlign = 'center';
table.style.fontFamily = 'Courier New';
for (i=1; i<=10; i++) {
    row = document.createElement('tr');
    for (j=1; j<=10; j++) {
        cell = document.createElement(i === 1 || j === 1 ? 'th' : 'td');
        cell.appendChild(document.createTextNode(i * j));
        cell.style.padding = '4px';
        cell.style.width = 100 / 10 + '%';
        row.appendChild(cell);
    }
    table.appendChild(row);
}
document.body.appendChild(table);


let table2, row2, cell2, i2, j2;
table2 = document.createElement('table');
table2.style.textAlign = 'center';
table2.style.fontFamily = 'Courier New';
for (i2 = 0; i2 <= 10; i2++) {
    row2 = document.createElement('tr');
    for (j2 = 0; j2 <= 10; j2++) {
        cell2 = document.createElement(i2 === 0 || j2 === 0 ? 'th' : 'td');
        cell2.appendChild(document.createTextNode(i2 + j2));
        cell2.style.padding = '4px 9px';
        cell2.style.width = 100 / 11 + '%';
        row2.appendChild(cell2);
    }
    table2.appendChild(row2);
}
document.body.appendChild(table2);